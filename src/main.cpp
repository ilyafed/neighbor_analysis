/* Main function
 */
#include <iostream>     // std::cout, std::fixed
#include <iomanip>      // std::setprecision
#include <string>
#include <vector>
#include "read.cpp"
#include "nucleus_coordinates.cpp"
#include "density_coordinates.cpp"
#include "write.cpp"

using namespace std;

#define BONDING_CUT 3.0 //bohr
#define DENSITY_CUT 0.67
#define DYNAMIC_N 1

char** find_arg(char** begin, char** end, const string &value){
    char ** iter = begin;
    while (iter != end && (string(*iter) != value)) iter++;
    return iter;
}

int cloud_chgcars(string name){
    std::vector< System > system(1);
    if ( read_vasp(name, system[0], true) )
        return 1;
    cerr << "successful read file" << endl;
    cerr << "min: " << system[0].grid.min() << ", max: " << system[0].grid.max() << endl;
    density_split(system[0].grid, 0.67);
    nucleus_coordinates(system, 1.5);
    compare_nucleus_density(system[0].particles, system[0].grid);
    // int* grid_shape = grid.shape();

    // vector<int> point(3);
    // for( point[0] = 0; point[0] < grid_shape[0]; point[0]++)
    //     for( point[1] = 0; point[1] < grid_shape[0]; point[1]++)
    //         for( point[2] = 0; point[2] < grid_shape[0]; point[2]++)
    //             cerr << grid.get_property("cloud", point) << " ";
    

    return 0;
}

int get_vasp(char** begin, char** end, std::vector<System> &system, bool density_flag){
    // check -vasp in arguments and read files
    char** iter;
    iter = find_arg(begin, end, "-vasp");
    std::vector<string> names;
    if (iter != end){
        iter ++;
        
        while (iter < end){
            names.push_back(*iter);
            iter++;
        }
        if (names.size()){
            read_several_vasp(names, system, density_flag);
            return 0;
        } else {
            cerr << "Need name of chgcar files: -vasp [filename1] [filename2] ...\n";
            return 1;
        }
    }
    return 0;
}

int get_trajectory(char** begin, char** end, std::vector<System> &system){
    // check -trajectory (CPMD) in arguments and read files
    char** iter;
    iter = find_arg(begin, end, "-trajectory");
    if (iter != end){
        iter ++;
        int read_step = 1;
        if ( string(*iter) == string("-step") ){
            iter ++;
            if (iter < end){
                read_step = stoi(*iter);
            }
            iter++;
        }
        std::vector<string> name;
        if (iter < end){
            name.push_back(*iter);
        } else {
            cerr << "Need name of trajectory file: -trajectory [filename]\n";
            return 1;
        }
        iter = find_arg(begin, end, "-wall");
        std::vector<double> wall;
        if (iter != end){
            if (++iter < end)
                wall.push_back(std::stod(*iter));
            else
                cerr << "Need x lattice coordinate\n";
            if (++iter < end)
                wall.push_back(std::stod(*iter));
            else
                cerr << "Need y lattice coordinate\n";
            if (++iter < end)
                wall.push_back(std::stod(*iter));
            else
                cerr << "Need y lattice coordinate\n";
        }

        if ( (name.size()==1) && (wall.size() == 3) ){
            read_trajectory(name[0], system, wall, read_step);
            return 0;
        } else {
            return 1;
        }
    }
    return 0;
}

int get_bonding(char** begin, char** end, std::vector<System> &system, double &bonding_cut, double &density_cut){
    // check -bonding -bonding_cut -density -density_cut
    char** iter;
    
    iter = find_arg(begin, end, "-bonding");
    bool bonding_flag = (iter != end);
    bonding_cut = BONDING_CUT;
    if (!bonding_flag){
        bonding_cut = 0;
        return 0;
    }
    iter = find_arg(begin, end, "-bonding_cut");
    bool bonding_cut_flag = (iter != end);
    if ( bonding_cut_flag ){
        if (++iter != end){
            bonding_cut = std::stod(*iter);
        }else{
            cerr << "error: need bonding cut [double]\n";
            return 1;
        }
    }

    iter = find_arg(begin, end, "-density");
    bool density_flag = (iter != end);
    density_cut = 0;
    if (density_flag)
        density_cut = DENSITY_CUT;

    iter = find_arg(begin, end, "-density_cut");
    bool density_cut_flag = (iter != end);
    if ( density_cut_flag ){
        if (++iter != end){
            density_cut = std::stod(*iter);
        }else{
            cerr << "error: need density cut [double]\n";
            return 1;
        }
    }

    return 0;
}

int get_dynamic(char** begin, char** end, std::vector<System> &system, int &dynamic_N_points, int &dynamic_N_every){
    // check -dynamic
    char** iter;
    
    iter = find_arg(begin, end, "-dynamic");
    bool dynamic_flag = (iter != end);
    dynamic_N_points = DYNAMIC_N;
    if (!dynamic_flag){
        dynamic_N_points = 0;
        return 0;
    }

    iter = find_arg(begin, end, "-average_points");
    bool dynamic_N_point_flag = (iter != end);
    if ( dynamic_N_point_flag ){
        if (++iter != end){
            dynamic_N_points = std::stoi(*iter);
        }else{
            cerr << "error: need -average_points N [int]\n";
            return 1;
        }
    }

    iter = find_arg(begin, end, "-average_point_step");
    bool dynamic_N_every_flag = (iter != end);
    if ( dynamic_N_every_flag ){
        if (++iter != end){
            dynamic_N_every = std::stoi(*iter);
        }else{
            cerr << "error: need -average_point_step N [int]\n";
            return 1;
        }
    }
    return 0;
}

int get_mod_flag(char** begin, char** end){
    // check -dynamic
    char** iter;
    
    iter = find_arg(begin, end, "-mod");
    bool mod_flag = (iter != end);
    return mod_flag;
}

int get_output(char** begin, char** end, std::vector<System> &system){
    // check -vasp in arguments and read files
    char** iter;
    iter = find_arg(begin, end, "-output");
    
    if (iter != end){
        iter ++;
        string type;
        if (iter < end)
            type = *iter;
        else
            return 1;
        iter++;

        if (iter < end){
            string name = *iter;
            size_t XXX_pos = name.find("XXX");
            if (XXX_pos == std::string::npos)
                write_lammps_data(name, system[0]);
            else{
                for (int i = 0 ; i < system.size(); i++){
                    //if (!system[i].set_types)
                    //    continue;
                    string newstring = name;
                    newstring.replace(XXX_pos, 3, to_string(system[i].step));
                    if (type == "lammps")
                        write_lammps_data(newstring, system[i]);
                    if (type == "xyz")
                        write_xyz(newstring, system[i]);
                }
            }
        }else{
            cerr << "Need name of output file: -output [filename1]\n";
            return 1;
        }
    }
    return 0;
}

string get_rdf(char** begin, char** end, std::vector<System> &system){
    // check -vasp in arguments and read files
    char** iter;
    iter = find_arg(begin, end, "-rdf");
    string filepath = "";
    if (iter != end){
        iter ++;
        if (iter < end){
            filepath = *iter;
        }
    } 

    return filepath;
}

string get_rdf_dynamic(char** begin, char** end, std::vector<System> &system){
    // check -vasp in arguments and read files
    char** iter;
    iter = find_arg(begin, end, "-rdf_dyn");
    string filename = "";
    if (iter != end){
        iter ++;
        if (iter < end){
            filename = *iter;
        }
    } 

    return filename;
}

int main(int argc, char* argv[]){    
    std::vector<System> system;

    // bonding by nucleus coordinates
    double bonding_cut=-1, density_cut=-1;
    if (get_bonding(argv, argv+argc, system, bonding_cut, density_cut)){
        return 0;
    }

    // check dynamic properties in argv
    int dynamic_N_every, dynamic_N_points;
    if (get_dynamic(argv, argv+argc, system, dynamic_N_points, dynamic_N_every)){
        return 0;
    }
    int mod_flag = get_mod_flag(argv, argv+argc);
    // read input files
    if (get_vasp(argv, argv+argc, system, ( density_cut == 0 ) ) ){
        cerr << "error: read VASP files\n";
        return 0;
    }

    if ( !system.size() )
        if (get_trajectory(argv, argv+argc, system)){
            cerr << "error: read TRAJECTORY file\n";
            return 0;
        }
    
    if ( !system.size() ){
        cerr << "error: no input parametrs\n";
        return 0;
    }

    string rdf_filepath = get_rdf(argv, argv+argc, system);
    string rdf_dyn_filename = get_rdf_dynamic(argv, argv+argc, system);

    //start bonding atoms if it's need
    if (bonding_cut != 0){
        nucleus_coordinates_dynamic(system, bonding_cut, density_cut, dynamic_N_every, dynamic_N_points, rdf_filepath, rdf_dyn_filename, mod_flag);
    }

    if (get_output(argv, argv+argc, system)){
        return 0;
    }
    

    // print help
    char **iter;
    iter = find_arg(argv, argv+argc, "-h");
    if (iter != argv+argc){
        cout << "At this moment, program has restricted functional" << endl;
        cout << " -several [filename1 filename2 ...]" << endl;
        cout << "    compare several chgcar files to extract neighbours by nucleus" << endl;
        cout << "    MUST BE THE LAST PROPERTY" << endl;
        cout << " -visual filename" << endl;
        cout << "    save xyz file with molecules types" << endl;
        cout << " -cloud filename" << endl;
        cout << "    split system into electron clound using electron density" << endl;
    }
    return 0;
}
