/* Compare nucleus coordinates and density of electrons to extract molecules type
 */
#ifndef __DENSITY_COORDINATES_CPP__
#define __DENSITY_COORDINATES_CPP__

#include <cmath>
#include <iostream>
#include <vector>
#include <list>
#include "additional_class.cpp"

using namespace std;

int find_cloud(Grid &grid, double r_cut, int k_flag, vector<int> &point){
    std::list<vector<int>> task_list;
    double mass = grid.mass(point);
    task_list.push_back(point);
    vector<int> x0;

    while (task_list.size()){
        for (int side = 0; side < 3; side ++)
            for (int step = -1; step <=1; step+= 2){
                x0 = task_list.front();
                x0[side] += step;
                if ( grid.get_property("cloud", x0) == 0 && grid.get(x0) > r_cut) {
                    mass += grid.mass(x0);
                    grid.set_property("cloud", x0, k_flag);
                    task_list.push_back(x0);
                }
            }
        task_list.pop_front();
    }
    grid.additional["mass_of_cloud"].push_back(mass);
    return 0;

}

int density_split(Grid &grid, double r_cut){
    // In this function we get grid and split it into clouds of density more then cut
    
    grid.property("cloud");

    int* grid_shape = grid.shape();

    // work with mass of cloud
    double mass_sum = 0; 
    grid.add_additional("mass_of_cloud");
    grid.add_additional("number_of_electrons");
    grid.additional["number_of_electrons"].push_back(0);
    grid.additional["mass_of_cloud"].push_back(0); // mass of k = 0 (rest mass)

    int k_flag = 1;
    vector<int> point(3);

    for( point[0] = 0; point[0] < grid_shape[0]; point[0]++)
        for( point[1] = 0; point[1] < grid_shape[0]; point[1]++)
            for( point[2] = 0; point[2] < grid_shape[0]; point[2]++){
                if ( grid.get_property("cloud", point) == 0 && grid.get(point) > r_cut) {
                    grid.set_property("cloud", point, k_flag);
                    find_cloud(grid, r_cut, k_flag, point);
                    mass_sum += grid.additional["mass_of_cloud"][k_flag];
                    k_flag ++;
                }
            }
    cerr << "number of clounds: " << k_flag-1 <<endl;
    double mass_coeff = grid.integrate()/mass_sum;
    grid.additional["mass_of_cloud"][0] = (mass_coeff - 1)/mass_coeff; // part of point less then r_cut
    cerr << "mass of points in grid which less then r_cut: "<< grid.additional["mass_of_cloud"][0] * 100 << "%\n";
    cerr << "cloud_id\tmass\taccepted_electron_number\n";
    for (int i = 1; i < grid.additional["mass_of_cloud"].size(); i++){
        grid.additional["mass_of_cloud"][i] *= mass_coeff;
        grid.additional["number_of_electrons"].push_back( round(grid.additional["mass_of_cloud"][i]));
        cerr << i << "\t" << grid.additional["mass_of_cloud"][i] << "\t" << (int) grid.additional["number_of_electrons"][i] << "\n";
    }

    return 0;
}

int compare_nucleus_density(std::vector<Particle> &coord, Grid &grid){
    int n = coord.size();
    cerr << "result of comparing\n";
    cerr << "ion_id\tnumber_of_neigbour_no_density\tnumber_of_neigbour_density_ion\telectron\n";

    for (int i = 0; i < n; i++){
        if (coord[i].type_element=="e")
            continue;

        vector<int> coord_point;
        
        coord_point = grid.get_point(coord[i].r);
        int k_flag = grid.get_property("cloud", coord_point);

        int number_of_ions = 1;
        int number_of_electrons = 0;
        
        if (k_flag)
            number_of_electrons = grid.additional["number_of_electrons"][k_flag];
        else
            number_of_electrons = 0;
        

    
        for (int i = 0; i < coord[i].neighbours.size(); i++){
            coord_point = grid.get_point(coord[i].r);
            if ( grid.get_property("cloud", coord_point) == k_flag){
                number_of_ions++;
            }
        }
        cerr << i << "\t" << coord[i].neighbours.size() + 1 << "\t" << number_of_ions << "\t" << number_of_electrons << "\n";
    }
    return 0;
}
#endif // __DENSITY_COORDINATES_CPP__