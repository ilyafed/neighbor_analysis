/* Function to read CHGCAR and CHG file from VASP 
 */
#ifndef __READ_CPP__
#define __READ_CPP__
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "additional_class.cpp"

using namespace std;

#define BOHR 0.529

double get_real(double value, double wall){
    while (value < 0){
        value += wall;
    }
    while (value >= wall){
        value -= wall;
    }
    return value;
}

int read_vasp(string filename, System &system, bool density_flag){
    // open CHGCAR file for reading
    ifstream infile(filename);
    setlocale(LC_ALL, "C");
    if (!infile) {
        cerr <<  "READ_VASP: cannot open " << filename << " file";
        return 1;
    }
    // initialisate variables
    string buf; // buffers
    double x,y,z; //buffers
    string name_of_system;
    double scale_factor;
    string name_of_element;
    int number_of_particles;
    string coordinates_type;
    int grid_shape[3];

    //1: get name of file
    getline(infile, name_of_system);

    //2: scale factor
    infile >> buf;
    scale_factor = stod(buf)/BOHR;
    //3,4,5: get vector of lattice a\n b\n c
    infile >> x >> y >> z;
    system.lattice.push_back( Vector(x,y,z) * scale_factor );
    infile >> x >> y >> z;
    system.lattice.push_back( Vector(x,y,z) * scale_factor );
    infile >> x >> y >> z;
    system.lattice.push_back( Vector(x,y,z) * scale_factor );

    //6: get name of element
    infile >> name_of_element;
    
    //7: get number of particles
    infile >> number_of_particles;

    //8: type of coordinates: Direct or Cortesian
    infile >> coordinates_type;
    if ((coordinates_type[0] != 'D') && (coordinates_type[0] != 'C')){
        cerr << "READ_VASP: error reading coordinate type: " << coordinates_type << endl;
        return 1;
    }

    //9: get coordinates of all nucleus
    for (int i = 0; i < number_of_particles; i++){
        infile >> x >> y >> z;
        if (coordinates_type[0]=='D')
            system.particles.push_back( Particle( (system.lattice[0]*x + system.lattice[1]*y + system.lattice[2]*z) * (1/BOHR), name_of_element));
        else
            system.particles.push_back( Particle(Vector(x,y,z) * scale_factor, name_of_element));

    }
    if ( !density_flag ){
        return 0;
    }
    //9+N+1: free line
    getline(infile, buf);

    //9+N+2: get size of grid
    infile >> grid_shape[0] >> grid_shape[1] >> grid_shape[2];
    
    //create grid with this size
    if (system.grid.len() != 0){
        std::cerr << "READ_VASP: Not empty Grid class" << endl;
        return 1;
    }

    system.grid.create(grid_shape);
    system.grid.set_lattice(system.lattice);
    double lattice_volume = system.lattice[0].mult(system.lattice[1].mult_vector(system.lattice[2]));
    vector<int> point(3);
    for( point[2] = 0; point[2] < grid_shape[0]; point[2]++)
        for( point[1] = 0; point[1] < grid_shape[0]; point[1]++)
            for( point[0] = 0; point[0] < grid_shape[0]; point[0]++){
                infile >> buf;
                system.grid.set(point, stod(buf)/lattice_volume);
            }
            
    infile.close();
    return 0;
}

int read_several_vasp(std::vector<string> &filenames, std::vector<System> &system, bool density_flag){
    for ( int i = 0; i < filenames.size(); i++){
        System current_system;
        read_vasp(filenames[i], current_system, density_flag);
        current_system.step = i;
        system.push_back(current_system);
    }
    return 0;
}

int read_trajectory(string filename, std::vector<System> &system, std::vector<double> wall, int skip_step = 1){
    // open CHGCAR file for reading
    ifstream infile(filename);
    setlocale(LC_ALL, "C");
    if (!infile) {
        cerr <<  "READ_TRAJECTORY: cannot open " << filename << " file";
        return 1;
    }
    int step, skip_step_iterator=0, previous_step=0, system_step;
    int expected_step = 0;
    double x, y, z, vx, vy, vz;
    std::vector<Vector> lattice(3, Vector(0,0,0));
    lattice[0].r[0] = wall[0];
    lattice[1].r[1] = wall[1];
    lattice[2].r[2] = wall[2];
    System current_system;
    //std::stringstream buf_stream;
    string step_buf; //, x_buf, y_buf, z_buf;
    //infile >> buf;
    //cout << "buf: " << buf << endl;
    int restart_step = 0;
    //buf_stream << infile.rdbuf();
    //infile.close();
    //TODO
    // int test_iterator = 0;
    while (infile >> step_buf) { // >> x >> y >> z >> vx >> vy >> vz) {
        // if (test_iterator > 2500) {
        //     exit (EXIT_FAILURE);
        //     break;
        // }
        //test_iterator +=1;
        if (step_buf == "<<<<<<"){
            cerr << "found new data" << endl;
            infile >> step_buf; // NEW
            infile >> step_buf; // DATA
            infile >> step_buf; // >>>>>>>
            restart_step = step-1+expected_step;
            continue;
        }
        step = std::stoi(step_buf);
        infile >> x >> y >> z >> vx >> vy >> vz;
        //cout << "string: " << buf << endl;
        //buf_stream << buf;
        //buf_stream >> step >> x >> y >> z >> vx >> vy >> vz;
        //cout << "result: " << step <<endl;
        //cerr << "Read first line " << step << endl;
        step += restart_step;

        // cout << step << endl;
        if (previous_step == 0){
            previous_step = step;
        }
        if (step > previous_step){
            skip_step_iterator ++;
            expected_step = (step - previous_step)/skip_step_iterator;
        }
        if (skip_step_iterator == skip_step){
            current_system.lattice = lattice;
            current_system.step = system_step;
            current_system.velocity_flag = true;
            system.push_back(current_system);            
            current_system = System();    
            skip_step_iterator = 0;
        }
        if (skip_step_iterator == 0){
            system_step = step;
            x = get_real(x, wall[0]);
            y = get_real(y, wall[1]);
            z = get_real(z, wall[2]);
            current_system.particles.push_back( Particle( x,y,z, vx, vy, vz, "unknown"));
        }
        previous_step = step;
    }
    current_system.lattice = lattice;
    current_system.step = system_step;
    current_system.velocity_flag = true;
    system.push_back(current_system);
    cerr << "last read_step: " << system_step <<endl;
    infile.close();
    return 0;
}

#endif // __READ_CPP__
