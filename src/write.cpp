/* Function to read CHGCAR and CHG file from VASP 
 */
#ifndef __WRITE_CPP__
#define __WRITE_CPP__
#include <iostream>
#include <fstream>
#include <iomanip>      // std::setprecision
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include "additional_class.cpp"

using namespace std;

int write_xyz(string filename, System &system, bool type_flag=true, bool type_element_flag=false){
    // open CHGCAR file for reading
    ofstream outfile(filename);
    setlocale(LC_ALL, "C");
    if (!outfile) {
        cerr <<  "WRITE_XYZ: cannot open " << filename << " file";
        return 1;
    }
    outfile << system.particles.size() << endl;
    outfile << "This file created by neigan programm" << endl;
    for (int i = 0; i < system.particles.size(); i++){
        if (type_flag)
            outfile << system.particles[i].type << " ";
        if (type_element_flag)
            outfile << system.particles[i].type_element << " ";
        
        outfile << system.particles[i].r.str() << endl;
    }
    outfile.close();
    return 0;
}

int write_lammps_data(string filename, System &system){
    ofstream outfile(filename);
    setlocale(LC_ALL, "C");
    if (!outfile) {
        cerr <<  "WRITE_LAMMPS_DATA: cannot open " << filename << " file";
        return 1;
    }

    outfile << "Date_file from neighbour\n\n";
    outfile << system.particles.size() << " atoms\n";
    int bond_count = 0;
    for (int i = 0; i < system.particles.size(); i++){
        for (int j = 0; j < system.particles[i].first_neighbours.size(); j++){
            if (system.particles[i].first_neighbours[j] > i){
                bond_count++;
            }
        }
    }
    outfile << bond_count << " bonds\n";

    std::map<std::string, int> mol_type;
    mol_type["H"] = 1;
    mol_type["H2"] = 2;
    mol_type["H3"] = 3;
    mol_type["-"] = 4;
    for (int i = 0; i < system.particles.size(); i++){
        if (mol_type.find(system.particles[i].type) == mol_type.end()){
            mol_type[system.particles[i].type] = mol_type.size()+1;
        }
    }
    outfile << mol_type.size() << " atom types\n";
    outfile << "1 bond types\n\n";

    

    outfile << "0 " << system.lattice[0].r[0] << " xlo xhi\n";
    outfile << "0 " << system.lattice[1].r[1] << " ylo yhi\n";
    outfile << "0 " << system.lattice[2].r[2] << " zlo zhi\n";

    outfile << "\nAtoms\n\n";
    double charge = 1;
    for (int i = 0; i < system.particles.size(); i++){
        outfile << i+1 << "\t" << mol_type[system.particles[i].type] << "\t" << mol_type[system.particles[i].type] << "\t"
            << fixed << setprecision(6) << charge << "\t" << system.particles[i].r.r[0] << "\t" 
            << system.particles[i].r.r[1] << "\t" << system.particles[i].r.r[2] << endl;
    }

    if (system.velocity_flag){
        outfile << "\nVelocities\n\n";
        for (int i = 0; i < system.particles.size(); i++){
            outfile << i+1 << "\t" << system.particles[i].v.r[0] << "\t" 
                << system.particles[i].v.r[1] << "\t" << system.particles[i].v.r[2] << endl;
        }
    }

    outfile << "\nBonds\n\n";
    
    int bond_type = 1, bond_number = 1;
    for (int i = 0; i < system.particles.size(); i++){
        for (int j = 0; j < system.particles[i].first_neighbours.size(); j++){
            if (system.particles[i].first_neighbours[j] > i){
                outfile << bond_number << "\t" << bond_type << "\t" << i+1 << "\t" << system.particles[i].first_neighbours[j]+1 << endl;
                bond_number++;
            }
        }
    }

    outfile.close();
    return 0;
}
#endif // __WRITE_CPP__