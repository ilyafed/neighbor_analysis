// 
/* Here we introduce some useful classes and structures
 */
#ifndef __ADDITIONAL_CLASS_CPP__
#define __ADDITIONAL_CLASS_CPP__
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <map>



struct Vector{
	double r[3];

	Vector(){}
	Vector(double x, double y, double z){
        this->r[0] = x, this->r[1] = y , this->r[2] = z;
    }
    Vector(std::string x, std::string y, std::string z){
        this->r[0]=std::stod(x), this->r[1] = std::stod(y), this->r[2] = std::stod(z);
    }
    
    Vector operator = (const Vector & v1){
        this->r[0] = v1.r[0], this->r[1] = v1.r[1], this->r[2] = v1.r[2];
        return *this;
    }

    Vector operator + (const Vector v1){
        Vector result(v1.r[0] + r[0], v1.r[1] + r[1], v1.r[2] + r[2]);   
        return result;
    }

    Vector operator - (const Vector v1){
        Vector result(v1.r[0] - r[0], v1.r[1] - r[1], v1.r[2] - r[2]);   
        return result;
    }

    const bool operator == (const Vector v1) const{
        return ((v1.r[0] == r[0]) && ( v1.r[1] == r[1]) && ( v1.r[2] == r[2]) );   
    }
    

    Vector operator * (const double scale){
        Vector result(r[0] * scale, r[1] * scale, r[2] * scale);   
        return result;
    }

    double mult(const Vector v1){
        return v1.r[0] * r[0] + v1.r[1] * r[1] + v1.r[2] * r[2];   
    }

    Vector mult_vector(Vector v1){
        Vector result(r[1]*v1.r[2] - r[2]*v1.r[1], - r[0]*v1.r[2] + r[2]*v1.r[0], r[0]*v1.r[1] - r[1]*v1.r[0]);
        return result;
    }

    double len(){
        return pow(this->mult(*this), 0.5);
    }

    std::string str(){
        return std::to_string(this->r[0]) + " " + std::to_string(this->r[1]) + " " + std::to_string(this->r[2]);
    }
};

class Grid{
typedef std::vector<double>      GridProperty;
typedef std::map<std::string, GridProperty> GridPropertyMap;

private:
    std::vector<double> _grid; // grid [N][N][N]
    GridPropertyMap _property;
    int _shape[3];
    int _len;
    std::vector<Vector> lattice;
    double cell_volume;

public:
    GridPropertyMap additional;

    Grid(int* _shape){
        this->create(_shape);
    }
    
    int create(int* _shape){
        this->_shape[0] = _shape[0];
        this->_shape[1] = _shape[1];
        this->_shape[2] = _shape[2];
        this->_len = this->_shape[0] * this->_shape[1] * this->_shape[2];
        _grid.resize(this->_len, 0);
        return 0;
    }
    int set_lattice(std::vector<Vector> &lattice){
        this->lattice.push_back(lattice[0]);
        this->lattice.push_back(lattice[1]);
        this->lattice.push_back(lattice[2]);
        this->cell_volume = lattice[0].mult(lattice[1].mult_vector(lattice[2])) / 
            ( this->_shape[0] * this->_shape[1] * this->_shape[2] );

        return 0;
    }

    Grid(){
        this->_shape[0] = 0;
        this->_shape[1] = 0;
        this->_shape[2] = 0;
        this->_len = 0;
    };
    ~Grid(){
    };

    int set(int i, int j, int k, double value){
        this->_grid[_shape[0] * _shape[1] * k + _shape[1] * j + i] = value;
        return 0;
    }
    double get(int i, int j, int k){
        return this->_grid[_shape[0] * _shape[1] * k + _shape[1] * j + i];
    }

    int set(std::vector<int> &point, double value){
        std::vector<int> real_point(3);
        for (int l = 0; l < 3; l++)
            real_point[l] = (point[l] + _shape[l]) % _shape[l];
        this->_grid[_shape[0] * _shape[1] * real_point[2] + _shape[1] * real_point[1] + real_point[0]] = value;
        return 0;
    }
    double get(std::vector<int> &point){
        std::vector<int> real_point(3);
        for (int l = 0; l < 3; l++)
            real_point[l] = (point[l] + _shape[l]) % _shape[l];
        return this->_grid[_shape[0] * _shape[1] * real_point[2] + _shape[1] * real_point[1] + real_point[0]];
    }

    double mass(std::vector<int> &point){
        std::vector<int> real_point(3);
        for (int l = 0; l < 3; l++)
            real_point[l] = (point[l] + _shape[l]) % _shape[l];
        return this->_grid[_shape[0] * _shape[1] * real_point[2] + _shape[1] * real_point[1] + real_point[0]] * this->cell_volume;
    }

    double integrate(){
        double sum = 0;
        for (int i = 0; i < this->_len; i++){
            sum += this->_grid[i];
        }
        sum *= this->cell_volume;
        return sum;
    }

    double min(){
        double min = 100.0;
        for (int i = 0; i < this->_len; i++){
            if ( this->_grid[i] < min ){
                min = this->_grid[i];
            }
        }
        return min;
    }
    
    double max(){
        double max = 0.0;
        for (int i = 0; i < this->_len; i++){
            if ( this->_grid[i] > max ){
                max = this->_grid[i];
            }
        }
        return max;
    }

    int* shape(){
        return this->_shape;
    }
    int len(){
        return this->_len;
    }

    int property(std::string name){
        this->_property.insert( GridPropertyMap::value_type(name, GridProperty(this->_len, 0)) );
        return 0;
    }

    double get_property(std::string name, std::vector<int> &point){
        //std::cerr << "get " << point[0] << " " << point[1] << " " << point[2] << std::endl;
        std::vector<int> real_point(3);
        for (int l = 0; l < 3; l++)
            real_point[l] = (point[l] + _shape[l]) % _shape[l];
        return this->_property[name][_shape[0] * _shape[1] * real_point[2] + _shape[1] * real_point[1] + real_point[0]];
    }
    int set_property(std::string name, std::vector<int> &point, double value){
        //std::cerr << "set " << point[0] << " " << point[1] << " " << point[2] << std::endl;
        std::vector<int> real_point(3);
        for (int l = 0; l < 3; l++)
            real_point[l] = (point[l] + _shape[l]) % _shape[l];
        this->_property[name][_shape[0] * _shape[1] * real_point[2] + _shape[1] * real_point[1] + real_point[0]] = value;
        return 0;
    }
    int add_additional(std::string name){
        this->_property.insert( GridPropertyMap::value_type(name, GridProperty() ));
        return 0;
    }

    double get(Vector r){
        std::vector<int> result(3,0);
        for (int l = 0; l < 3; l++){
            result[l] = r.mult(lattice[l])/pow(this->lattice[l].len(),2)*this->_shape[l];
        }
        // make periodic
        for (int l = 0; l < 3; l++){
            result[l] = (result[l] + this->_shape[l]) % this->_shape[l];
        }
        return this->get(result);
    }
    std::vector<int> get_point(Vector r){
        std::vector<int> result(3,0);
        for (int l = 0; l < 3; l++){
            result[l] = r.mult(lattice[l])/pow(this->lattice[l].len(),2)*this->_shape[l];
        }
        // make periodic
        for (int l = 0; l < 3; l++){
            result[l] = (result[l] + this->_shape[l]) % this->_shape[l];
        }
        return result;
    }

    double min_line(Vector r1, Vector r2){
        int n = 10;
        Vector step((r2-r1)*(1.0/n));
        double min = 100.0;
        double value;
        for (int i = 0; i < n; i++){
            value = this->get(r1 + step*n);
            if ( value < min )
                min = value;
        }
        return min;
    }


};



struct Particle{
	Vector r;
    Vector v;
    std::string type_element;
    std::string type;
    std::vector<int> neighbours;
    std::vector<int> first_neighbours;
    int number_of_ions = 0;
    int number_of_electrons = 0;

    Particle(double x, double y, double z, std::string type_element){
        this->r = Vector(x,y,z);
        this->type_element = type_element;
    }
    Particle(double x, double y, double z, double vx, double vy, double vz, std::string type_element){
        this->r = Vector(x,y,z);
        this->v = Vector(vx,vy,vz);
        this->type_element = type_element;
    }
    Particle(Vector r, std::string type_element){
        this->r = r;
        this->type_element = type_element;
    }
    Particle(Vector r){
        this->r = r;
        this->type_element = "unknown";
    }
};

struct System{
    std::vector<Particle> particles;
    Grid grid;
    std::vector<Vector> lattice;
    bool electron = false;
    int step;
    int h_h2_mod[2] = {0,0};
    bool set_types = false;
    int rdf_size = 100;
    std::vector<double> rdf;
    bool velocity_flag = false;
};
#endif // __ADDITIONAL_CLASS_CPP__
