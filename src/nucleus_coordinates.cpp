/* Compare nucleus coordinates and extract molecules type
 */
#ifndef __NUCLEUS_COORDINATES_CPP__
#define __NUCLEUS_COORDINATES_CPP__

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>      // std::setprecision
#include <vector>
#include <list>
#include "additional_class.cpp"

using namespace std;

int set_types(System &system);

Vector get_nearest_neighbors(Vector particle, Vector neighbour, std::vector<Vector> &lattice){
    // get all neighbors for periodic conditions for that neighbour which less that r_cut

    Vector min = neighbour;
    double min_r = (particle-neighbour).len();

    double r_value;
    Vector r;
    for (int l = 0; l < 3; l ++){
        for (int side = -1; side <= 1; side += 2){
            r = neighbour + lattice[l] * side;
            r_value = (particle-r).len();
            if ( r_value < min_r ){
                min = r;
                min_r = r_value;
            }
        }
    }
    
    return min;
}

int find_neighbour( vector<int> &k_list, vector< System > &system, double r_cut, int istart, int k_flag, double density_bond, int begin = 0, int end = 1, int dynamic_N_every = 1){

    // "recursive" algorithm to find all neigbour
    std::list<int> task_list;
    k_list[istart] = k_flag;
    task_list.push_back(istart);
    
    while (task_list.size()){
        int i0 = task_list.front();
        for (int i = 0; i < system[begin].particles.size(); i++){
            if ( k_list[i] != 0 || i==i0)
                continue;
            
            int number_of_hits = 0;
            int number_of_hits_need = 0;
            double dist;

            for (int l = begin; l < end; l+= dynamic_N_every){
                number_of_hits_need ++;
                Vector neighbour = get_nearest_neighbors(system[l].particles[i0].r, system[l].particles[i].r,  system[l].lattice);
                dist = (neighbour - system[l].particles[i0].r).len();

                if ( dist < r_cut ){
                    system[begin].rdf[int(dist/r_cut*system[begin].rdf_size)] ++;
                    if (density_bond != 0){
                        Vector middle_point = ( neighbour + system[l].particles[i0].r ) * 0.5;
                        if ( system[l].grid.get(middle_point) > density_bond ){
                            number_of_hits ++;
                        } else {
                            break;
                        }
                    } else {
                        number_of_hits += 1;
                    }
                } else {
                    break;
                }
            }

            if ( number_of_hits == number_of_hits_need ) {
                
                task_list.push_back(i);
                k_list[i] = k_flag;
                system[begin].particles[i0].first_neighbours.push_back(i);
            }
        
        }
        task_list.pop_front();
    }
    return 0;
}

int nucleus_coordinates( vector< System > &system, double r_cut, double density_bond = 0, int begin = 0, int end = 1, int dynamic_N_every = 1){
    /* In this function we: 
        1) get two lists of Particles from two different steps
        2) find particles in first step which exist near then r_cut 
            (it also include particles which r > r_cut, but it have common neighbour with r1 and r2 < r_cut)
            and extract it to list
        3) for this list of neighbours we check if it remain neighbour in the second step, if not we delete it from list
        system[0].electron - is a flag that means that we should take into account electron coordinates (if it exists)
    */
    //cerr << "from " << system[begin].step << " to " << system[end].step << " with " << dynamic_N_every << " steps\n";
    //TODO system[0].electron work slow need to clean list from electrons
    int n = system[begin].particles.size();
    for (int l = begin; l < end; l+= dynamic_N_every)
        if (system[l].particles.size() != n){
            cerr << "NUCLEUS_COORDINATES: different sizes of particles lists" << endl;
            return 1;
        }
    system[begin].rdf.reserve(system[begin].rdf_size);
    for (int i = 0; i < system[begin].rdf_size; i++){
        system[begin].rdf[i] = 0;
    }

    vector<int> k_list(n,0);

    //r_cut = r_cut*r_cut; // we will compare dist^2


    int k_flag = 1;
    for (int i = 0; i < n; i ++){
        if (k_list[i] == 0){
            find_neighbour(k_list, system, r_cut, i, k_flag, density_bond, begin, end, dynamic_N_every);
            k_flag++;
        }
    }

    // now create a list with number of particles for every types:

    for (int i = 0; i < n; i++) //clear neighbour list if it exist 
        system[begin].particles[i].neighbours.clear();
    
    for (int i = 0; i < n; i++){
        if (!system[begin].electron && (system[begin].particles[i].type_element=="e"))
            continue;
        if (system[begin].particles[i].neighbours.size() != 0) // if we already create neighbor
            continue;

        k_flag = k_list[i];
        int number_of_electrons = 0;
        int number_of_ions = 0;

        if (system[begin].particles[i].type_element != "e")
            number_of_ions ++;
        else
            number_of_electrons ++;

        for (int j = i+1; j < n; j++){
            if (!system[begin].electron && (system[begin].particles[i].type_element=="e"))
                continue;
            if (k_list[j] == k_flag){
                system[begin].particles[i].neighbours.push_back(j);    
                if (system[begin].particles[i].type_element != "e")
                    number_of_ions ++;
                else
                    number_of_electrons ++;
            }
        }

        system[begin].particles[i].number_of_ions = number_of_ions;
        system[begin].particles[i].number_of_electrons = number_of_electrons;
        for (int j = 0; j < system[begin].particles[i].neighbours.size(); j++){
            system[begin].particles[ system[begin].particles[i].neighbours[j] ].neighbours = system[begin].particles[i].neighbours;
            system[begin].particles[ system[begin].particles[i].neighbours[j] ].number_of_ions = system[begin].particles[i].number_of_ions;
            system[begin].particles[ system[begin].particles[i].neighbours[j] ].number_of_electrons = system[begin].particles[i].number_of_electrons;
        }
    }

    // Get H and H2 as N_part_in_mol % 2 -> H; int(N_part_in_mol/2)*2 -> H2
    int n_h2_mod=0;
    int n_h_mod=0;
    int k_current=0;
    for (int i = 0; i < n; i++){
        k_flag = k_list[i];
        if (k_flag <= k_current){
            continue;
        }
        k_current = k_flag;
        int n_part = 1;
        for (int j = i+1; j < n; j++){
            if (k_list[j]  == k_flag){
                n_part ++;
            }
        }
        n_h2_mod += int(n_part/2)*2;
        n_h_mod += n_part % 2;
    }
    system[begin].h_h2_mod[0] = n_h_mod;
    system[begin].h_h2_mod[1] = n_h2_mod;
    set_types(system[begin]);
    return 0;
}

int set_types(System &system){
    // we will sign molecules only of this types: 
    // without electrone_flag: H, H2, H3, unknown
    // with electrone_flag: e, H+, H, H-, H2+, H2, H2-, H3+, H3, unknown
    if (system.electron){
        for (int i = 0; i < system.particles.size(); i++){
            int number_of_ions = system.particles[i].number_of_ions;
            int number_of_electrons = system.particles[i].number_of_electrons;
            if ( (number_of_electrons == 1) && (number_of_ions == 0) ){
                system.particles[i].type = "e";
            }else if ( (number_of_electrons == 0) && (number_of_ions == 1) ){
                system.particles[i].type = "H+";
            }else if ( (number_of_electrons == 1) && (number_of_ions == 1) ){
                system.particles[i].type = "H";
            }else if ( (number_of_electrons == 2) && (number_of_ions == 1) ){
                system.particles[i].type = "H-";
            }else if ( (number_of_electrons == 1) && (number_of_ions == 2) ){
                system.particles[i].type = "H2+";
            }else if ( (number_of_electrons == 2) && (number_of_ions == 2) ){
                system.particles[i].type = "H2";
            }else if ( (number_of_electrons == 3) && (number_of_ions == 2) ){
                system.particles[i].type = "H2-";
            }else if ( (number_of_electrons == 2) && (number_of_ions == 3) ){
                system.particles[i].type = "H3+";
            }else if ( (number_of_electrons == 3) && (number_of_ions == 3) ){
                system.particles[i].type = "H3";
            }else{ 
                system.particles[i].type = "-";
            }
        }
    }else{
        for (int i = 0; i < system.particles.size(); i++){
            int number_of_ions = system.particles[i].number_of_ions;
            if (number_of_ions == 1){
                system.particles[i].type = "H";
            }else if (number_of_ions == 2){
                system.particles[i].type = "H2";
            }else if (number_of_ions == 3){
                
                system.particles[i].type = "H3";
            }else{ 
                system.particles[i].type = "-";
            }
        }
    }
    system.set_types = true;
    return 0;
}

int print_count_mod(string filename, System &system, bool first_flag = false, bool print_flag = true){
    
    if (first_flag){
        if (print_flag)
            cout << "step\tH\tH2\n";
        ofstream outfile;
        outfile.open(filename, std::ofstream::out);
        outfile << "step\tH\tH2\n";
        outfile.close();
        return 0;
    }
    ofstream outfile;
    if (first_flag)
        outfile.open(filename, std::ofstream::out);
    else
        outfile.open(filename, std::ofstream::out | std::ofstream::app);
    
    double h_count = system.h_h2_mod[0];
    double h2_count = system.h_h2_mod[1];
    int n = system.particles.size();
    h_count /= n;
    h2_count /= n;
    
    if (print_flag)
        cout << system.step << "\t" << std::fixed << setprecision(3) << h_count << "\t" << h2_count << endl;
    outfile << system.step << "\t" << std::fixed << setprecision(3) <<  h_count << "\t" << h2_count << endl;

    outfile.close();
    return 0;
}

int print_count(string filename, System &system, bool first_flag = false, bool print_flag = true){
    
    if (first_flag){
        if (print_flag)
            cout << "step\tH\tH2\tH3\tunknown\n";
        ofstream outfile;
        outfile.open(filename, std::ofstream::out);
        outfile << "step\tH\tH2\tH3\tunknown\n";
        outfile.close();
        return 0;
    }
    ofstream outfile;
    if (first_flag)
        outfile.open(filename, std::ofstream::out);
    else
        outfile.open(filename, std::ofstream::out | std::ofstream::app);
    
    double h_count = 0;
    double h2_count = 0;
    double h3_count = 0;
    double unknown_count = 0;
    int n = system.particles.size();
    for (int i = 0; i < n; i++){
        if (system.particles[i].type == "H")
            h_count ++;
        else if (system.particles[i].type == "H2")
            h2_count ++;
        else if (system.particles[i].type == "H3")
            h3_count ++;
        else if (system.particles[i].type == "-")
            unknown_count ++;
        //cerr << i << " " << h_count << endl;
    }
    
    h_count /= n;
    h2_count /= n;
    h3_count /= n;
    unknown_count /= n;
    
    if (print_flag)
        cout << system.step << "\t" << std::fixed << setprecision(3) << h_count << "\t" << h2_count << "\t" << h3_count << "\t" << unknown_count << endl;
    outfile << system.step << "\t" << std::fixed << setprecision(3) <<  h_count << "\t" << h2_count << "\t" << h3_count << "\t" << unknown_count << endl;

    outfile.close();
    return 0;
}

int print_rdf(string filename, System &system, double r_cut){
    ofstream outfile;
    outfile.open(filename, std::ofstream::out);
    
    
    outfile << "r\trdf\n";
    for (int i = 0; i < system.rdf_size; i++){
        outfile << (double) i/system.rdf_size * r_cut << "\t" << system.rdf[i] << "\n";
    }

    outfile.close();
    return 0;
}

int print_rdf_dyn(string filename, System &system, double r_cut, bool first_flag=false){
    ofstream outfile;
    if (first_flag){
        outfile.open(filename, std::ofstream::out);
        outfile << "step\tr\trdf\n";
        outfile.close();
        return 0;
    }
    outfile.open(filename, std::ofstream::out | std::ofstream::app);
    
    
    for (int i = 0; i < system.rdf_size; i++){
        outfile << system.step << "\t" << (double) i/system.rdf_size * r_cut << "\t" << system.rdf[i] << "\n";

        //if (system.rdf[i] > 0){
        //    outfile << system.step << "\t" << (double) i/system.rdf_size * r_cut << "\n";
        //}
    }

    outfile.close();
    return 0;
}

int nucleus_coordinates_dynamic( vector< System > &system, double bonding_cut, double density_cut = 0, int dynamic_N_every =1, int dynamic_N_points = 1, string rdf_filepath="", string rdf_dyn_filename="", int mod_flag=0){

    if (mod_flag)
        print_count_mod("neigan.txt", system[0]);
    else
        print_count("neigan.txt", system[0]);

    if (rdf_dyn_filename != "")
        print_rdf_dyn(rdf_dyn_filename, system[0], bonding_cut, true);

    // if ( dynamic_N_points == 0){
    //     nucleus_coordinates(system, bonding_cut, density_cut, 0, system.size(), 1);
    //     if (rdf_filepath != "")
    //         print_rdf(rdf_filepath + "/rdf.txt", system[0], bonding_cut);
    //     if (rdf_dyn_filename != "")
    //         print_rdf_dyn(rdf_dyn_filename, system[0], bonding_cut);
    //     print_count("neigan.txt", system[0]);
    //     return 0;
    // }
    for (int i = 0; i < system.size()-dynamic_N_every*dynamic_N_points; i++){
        nucleus_coordinates(system, bonding_cut, density_cut, i, i+dynamic_N_every*dynamic_N_points+1, dynamic_N_every);
        if (rdf_filepath != "")
            print_rdf(rdf_filepath + "/rdf." + to_string(system[i].step) + ".txt", system[i], bonding_cut);
        if (rdf_dyn_filename != "")
            print_rdf_dyn(rdf_dyn_filename, system[i], bonding_cut);
        if (mod_flag)
            print_count_mod("neigan.txt", system[i]);
        else
            print_count("neigan.txt", system[i]);
    }

    return 0;
}


#endif // __NUCLEUS_COORDINATES_CPP__