#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <iostream>

using namespace std;

struct globalArgs_t {
    int noIndex;                /* параметр -I */
    char *langCode;             /* параметр -l */
    const char *outFileName;    /* параметр -o */
    FILE *outFile;
    int verbosity;              /* параметр -v */
    char **inputFiles;          /* входные файлы */
    int numInputFiles;          /* число входных файлов */
    int randomized;             /* параметр --randomize */
} globalArgs;
 
static const char *optString = "Il:o:vh?";
 
static const struct option longOpts[] = {
    { "dynamic", no_argument, NULL, 0},
    { "step", required_argument, NULL, 's' },
    { "amount", required_argument, NULL, 'n' },
    { "timestep", no_argument, NULL, 't' },
    { "filetype", no_argument, NULL, 'f' },
    { "output", no_argument, NULL, 'o' },
    { "wall-cub", no_argument, NULL, 'w' },
    { "wall_a", no_argument, NULL, 'a' },
    { "wall_b", no_argument, NULL, 'b' },
    { "wall_c", no_argument, NULL, 'c' },
    { "bonding", no_argument, NULL, 0},
    { "bonding-cut", no_argument, NULL, 'r' },
    { "density", no_argument, NULL, 0},
    { "density-cut", no_argument, NULL, 'd' },
    { "help", no_argument, NULL, 'h' },
    { NULL, no_argument, NULL, 0 }
};

void display_usage( void )
{
    cout << "Help:\n";
    cout << "-dynamic [-d]; for every step ${j} we read ${j}+${i}x${st} for i in [0,...,${am}] to average data\n";
    cout << "\t-step [-s] st\n";
    cout << "\t-amount [-n] am\n";
    cout << "-timestep [-t] ts; timestep of our TRAJECTORY\n";
    cout << "-filetype [-f] ft [cpmd, vasp]; type of input file\n";
    cout << "-output [-o] output; output file name\n";
    cout << "-wall-cub [-w] wall; wall size in datafile format\n";
    cout << "-wall_a [-a] wall_a\n";
    cout << "-wall_b [-b] wall_b\n";
    cout << "-wall_c [-c] wall_c\n";
    cout << "-bonding [-b]; bonding molecules\n";
    cout << "\t-bonding-cut [-r] rcut; distance cut for molecules determination\n";
    cout << "-density [-d]; bonding molecules by density\n";
    cout << "\t-density-cut [-e]; density cut\n";
    exit( EXIT_FAILURE );
}
 
void run( void )
{
    /* ... */
}

int main( int argc, char *argv[] )
{
    int opt = 0;
     
    /* инициализация globalArgs до начала работы с ней. */
    globalArgs.noIndex = 0;     /* false */
    globalArgs.langCode = NULL;
    globalArgs.outFileName = NULL;
    globalArgs.outFile = NULL;
    globalArgs.verbosity = 0;
    globalArgs.inputFiles = NULL;
    globalArgs.numInputFiles = 0;

    opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
    while( opt != -1 ) {
        switch( opt ) {
            case 'I':
                globalArgs.noIndex = 1; /* true */
                break;
                 
            case 'l':
                globalArgs.langCode = optarg;
                break;
                 
            case 'o':
                globalArgs.outFileName = optarg;
                break;
                 
            case 'v':
                globalArgs.verbosity++;
                break;
                 
            case 'h':   /* намеренный проход в следующий case-блок */
            case '?':
                display_usage();
                break;
 
             case 0:     /* длинная опция без короткого эквивалента */
             if( strcmp( "randomize", longOpts[longIndex].name ) == 0 ) {
                    globalArgs.randomized = 1;
                }
                break;
                 
            default:
                /* сюда попасть невозможно. */
                break;
        }
         
        opt = getopt_long( argc, argv, optString, longOpts, amp;longIndex );
    }
     
    globalArgs.inputFiles = argv + optind;
    globalArgs.numInputFiles = argc - optind;

    run();
     
    return EXIT_SUCCESS;
}
